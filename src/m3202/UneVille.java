package m3202;

import java.io.Serializable;
import java.math.BigDecimal;

public class UneVille implements Serializable {
	private int num;
	private double coordX;
	private double coordY;
	
	public int getNum() {
		return num;
	}
	public double getCoordX() {
		return coordX;
	}
	public double getCoordY() {
		return coordY;
	}
	public UneVille(double coordX, double coordY,int num) {
		super();
		this.coordX = coordX;
		this.coordY = coordY;
		this.num = num;
	}
	public static double distance(UneVille v1,UneVille v2) {
		double xy = Math.pow((v1.getCoordX()-v2.getCoordX()),2)+Math.pow((v1.getCoordY()-v2.getCoordY()), 2);
		return Math.sqrt(xy);
	}
	@Override
	public String toString() {
		return "\nUneVille "+num+" [coordX=" + coordX + ", coordY=" + coordY + "]";
	}
}
