package m3202;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class EcrireVille {
	public static void main(String[] args) {
		List<UneVille> lv = uneListDeVille();
		Genetique g = new Genetique(test02(lv));
		System.out.println(Genetique.getNum(test02(lv)));
		double min = 1000;
		List<UneVille> chemin = new ArrayList<UneVille>();
		for (int i = 0; i < 100; i++) {
			g.createFils();
			if(Calaculatrice.distanceTotal(g.getMinimum())<min) {
				min = Calaculatrice.distanceTotal(g.getMinimum());
				chemin = g.getMinimum();
			}
		}
		System.out.println(min);
		System.out.println(Genetique.getNum(chemin));

	}
	public static List<UneVille> uneListDeVille(){
		List<UneVille> lv = null;
		try (ObjectInputStream ois = new ObjectInputStream(
			new FileInputStream("E:/Java/Eclipse2020-06/M3202/src/m3202/ville"));) {
			lv = (ArrayList<UneVille>) ois.readObject();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lv;
	}
	public static List<UneVille> dixVille(List<UneVille> l){
		Random r = new Random();
		Set<UneVille> s = new HashSet<UneVille>();
		while (s.size()<10) {
			int random = r.nextInt()%l.size();
			int index = random<0?random+l.size():random;
			s.add(l.get(index));
		}
		List<UneVille> l10 = new ArrayList<UneVille>();
		for(UneVille v : s) {
			l10.add(v);
		}
		return l10;
	}
	
	public static List<UneVille> test01(List<UneVille> lg){
		List<UneVille> l = new ArrayList<UneVille>();
		l.add(lg.get(47));
		l.add(lg.get(76));
		l.add(lg.get(137));
		l.add(lg.get(111));
		l.add(lg.get(221));
		l.add(lg.get(205));
		l.add(lg.get(59));
		l.add(lg.get(135));
		l.add(lg.get(179));
		l.add(lg.get(107));
		return l;
	}
	public static List<UneVille> test02(List<UneVille> lg){
		List<UneVille> l = new ArrayList<UneVille>();
		l.add(lg.get(0));
		l.add(lg.get(1));
		l.add(lg.get(2));
		l.add(lg.get(3));
		l.add(lg.get(4));
		l.add(lg.get(5));
		l.add(lg.get(6));
		l.add(lg.get(7));
		l.add(lg.get(8));
		l.add(lg.get(9));
		return l;
	}
}
