package m3202;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Genetique {
	private List<UneVille> parentA;
	private List<UneVille> parentB;
	private double pMutation;
	private List<UneVille> fils1;
	private List<UneVille> fils2;
	private Random rnd = new Random();;
	
	
	public Genetique(List<UneVille> parent) {
		parentA = new ArrayList<UneVille>();
		parentB = new ArrayList<UneVille>();
		this.parentA.addAll(parent);
		Collections.shuffle(parent);
		this.parentB.addAll(parent);		
	}

	public void createFils() {
		this.pMutation = this.rnd.nextDouble();
		int rndNDBorde =rnd.nextInt()%parentA.size();
		int PointCut = rndNDBorde<0?rndNDBorde+parentA.size():rndNDBorde;
		List<UneVille> A1 = new ArrayList<>();
		List<UneVille> A2 = new ArrayList<>();
		List<UneVille> B1 = new ArrayList<>();
		List<UneVille> B2 = new ArrayList<>();
		A1.addAll(parentA.subList(0, PointCut));
		A2.addAll(parentA.subList(PointCut,parentA.size()));
		B1.addAll(parentB.subList(0, PointCut));
		B2.addAll(parentB.subList(PointCut,parentB.size()));
		List<UneVille> ARaB = new ArrayList<UneVille>();
		List<UneVille> BRaA = new ArrayList<UneVille>();
		for(UneVille v:B2) {
			if(A1.contains(v)) {
				ARaB.add(v);
			}else {
				A1.add(v);
			}
		}
		for(UneVille v:A2) {
			if(B1.contains(v)) {
				BRaA.add(v);
			}else {
				B1.add(v);
			}
		}
		B1.addAll(ARaB);
		A1.addAll(BRaA);
		fils1 = A1;
		fils2 = B1;
//		if(pMutation>0.5) {
//			mutation();
//		}
	}
		
	public void mutation() {
		int random1 = rnd.nextInt()%fils1.size();
		int random2 = rnd.nextInt()%fils1.size();
		int index1 = random1<0?random1+fils1.size():random1;
		int index2 = random2<0?random2+fils1.size():random2;
		Collections.swap(fils1, index1, index2);
		Collections.swap(fils2, index1, index2);
	}

	public static List<Integer> getNum(List<UneVille> l){
		List<Integer> lnum = new ArrayList<Integer>();
		for(UneVille v : l) {
			lnum.add(v.getNum());
		}
		return lnum;
	}
	
	private double getDistance(List<UneVille> l) {
		return Calaculatrice.distanceTotal(l);
	}
	
	public List<UneVille> getMinimum(){
		List<UneVille> l= new ArrayList<UneVille>();
		List<UneVille> l2= new ArrayList<UneVille>();
		if(getDistance(parentA)<getDistance(parentB)) {
			l = parentA;
		}else {
			l = parentB;
		}
		if(getDistance(fils1)<getDistance(fils2)) {
			l2 = fils1;
		}else {
			l2 = fils2;
		}
		if(getDistance(l)<getDistance(l2)) {
			return l;
		}else {
			return l2;
		}
	}
	
	@Override
	public String toString() {
		return "Genetique \n[parentA=" + getNum(parentA) +"\n distance "+ getDistance(parentA)+
					 "\n parentB=" + getNum(parentB) +"\n distance "+ getDistance(parentB)+
					 "\n fils1=" + getNum(fils1) +"\n distance "+ getDistance(fils1)+
					 "\n fils2=" + getNum(fils2) +"\n distance "+ getDistance(fils2)+"]\n";
	}
	
}
