package m3202;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class LireVille {
	public static void main(String[] args) {
		File csv = new File("E:/Java/Eclipse2020-06/M3202/src/m3202/defi250.txt");
		try (BufferedReader br = new BufferedReader(new FileReader(csv));
				ObjectOutputStream oos = 
						new ObjectOutputStream(new FileOutputStream("E:/Java/Eclipse2020-06/M3202/src/m3202/ville"))){
			List<UneVille> lv = new ArrayList<>();
			String location = null;
			UneVille v = null;
			int num = 1;
			while((location = br.readLine())!=null) {
				String x = location.substring(0,17);
				String y = location.substring(18,35);
				Double dx = Double.valueOf(x);
				Double dy = Double.valueOf(y);
				v = new UneVille(dx, dy,num);
				lv.add(v);
				num++;
			}
			oos.writeObject(lv);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
