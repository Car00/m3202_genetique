package m3202;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Calaculatrice {
	public static double distanceTotal(List<UneVille> lv) {
		Iterator<UneVille> iter1 = lv.iterator();
		UneVille v1 = iter1.next();
		Iterator<UneVille> iter2 = lv.iterator();
		UneVille v2 = null;
		double distance = 0;
		for(;iter1.hasNext();) {
			v1 = iter1.next();
			v2 = iter2.next();
			distance += UneVille.distance(v1, v2);
		}
		distance+=UneVille.distance(lv.get(0),lv.get(lv.size()-1));
		return distance;
	}
	public static List<UneVille> tireDixVille(List<UneVille> lv){
		Random rnd = new Random();
		List<UneVille> lv2 = new ArrayList<UneVille>();
		int rndInt;
		int index;
		for (int i = 0; i < 10; i++) {
			rndInt = rnd.nextInt()%lv.size();
			index = rndInt>=0?rndInt:(rndInt+lv.size());
			lv2.add(lv.get(index));
		}
		return lv2;
	}
	
}
